
public class Aluno extends Pessoa {
	private String curso;

	public Aluno(String nome, int idade, String curso) {
		super(nome, idade);
		this.curso = curso;
	}

	public String getCurso() {
		return curso;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}

	@Override
	public String mostraDados() {
		String retornoNome = "Nome do aluno: " + this.nome + "\n";
		String retornoIdade = "Idade do aluno: " + this.idade + "\n";
		String retornoCurso = "Curso do aluno: " + this.curso;
		
		return retornoNome + retornoIdade + retornoCurso;
	}
	
}


public class Pessoa {
	protected String nome;
	protected int idade;
	
	public Pessoa(String nome, int idade) {
		setNome(nome);
		setIdade(idade);
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	
	public String mostraDados() {
		String retornoNome = "Nome da pessoa: " + this.nome + "\n";
		String retornoIdade = "Idade da pessoa: " + this.idade;
		
		return retornoNome + retornoIdade;
	}
}

import java.util.InputMismatchException;
import java.util.Scanner;

public class Tela {

	public static void main(String[] args) {

		String nome = "Janete";
		int idade = 25;
		String curso = "An�lise e Desenvolvimento de Sistemas";
		
		Scanner teclado = new Scanner(System.in);
		byte opcao = 0;
		
		try {
			System.out.println("O que deseja instanciar?");
			System.out.println("Digite 1 para Pessoa");
			System.out.println("Digite 2 para Aluno");
			opcao = teclado.nextByte();
			switch (opcao) {
			case 1:
				Pessoa pessoa = new Pessoa(nome, idade);
				System.out.println(pessoa.mostraDados());
				break;
			case 2:
				Aluno aluno = new Aluno(nome, idade, curso);
				System.out.println(aluno.mostraDados());
				break;
			}
		} catch (InputMismatchException e) {
			System.out.println("Valor informado n�o � v�lido");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
